import React from 'react';
import {Switch} from "react-router";
import LoginForm from "./LoginForm";
import Route from "react-router/Route";
import BlogPostListContainer from "./BlogPostListContainer";

class App extends React.Component {
  render() {
    return (
      <div>Hi app
        <Switch>
          <Route path="/login" component={LoginForm}/>
          <Route path="/" component={BlogPostListContainer}/>
        </Switch>
      </div>
    )
  }
}

export default App;
